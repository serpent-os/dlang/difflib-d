/* SPDX-License-Identifier: Zlib */

/**
 * difflib.types
 *
 * Simple structs and aliases for difflib.
 *
 * Authors: Copyright © 2022 Serpent OS Developers
 * License: Zlib
 */

module difflib.types;

import std.typecons : Nullable;

/**
 * A Match is returned by SequenceMatcher to help
 * understand offsets and such for a matching block.
 */
public struct Match
{
    /**
     * Offset within primary range
     */
    ulong primary;

    /**
     * Offset within secondary range
     */
    ulong secondary;

    /**
     * Length of the match
     */
    ulong size;

    /* Python API expectations */
    alias a = primary;
    alias b = secondary;
}

/**
 * We use OptionalMatch to allow isNull() testing
 */
public alias OptionalMatch = Nullable!(Match, Match.init);

/**
 * Simple definition of an Offset
 */
public struct Offset
{
    /**
     * The start of this offset
     */
    ulong start;

    /**
     * The end of this offset
     */
    ulong end;

    /**
     * Length is an implicit property
     *
     * Returns: The length of the offset (end - start)
     */
    pragma(inline, true) pure @property auto length() @safe @nogc nothrow const
    {
        return end - start;
    }

    /* Pythonic API expectations */
    alias lo = start;
    alias hi = end;
}

/**
 * MatchOffsets is used to control search locations
 */
public struct MatchOffsets
{
    /**
     * Offsets of the primary range
     */
    Offset primary;

    /**
     * Offsets of the secondary range
     */
    Offset secondary;

    /* Pythonic API expectations */
    alias a = primary;
    alias b = secondary;
}
