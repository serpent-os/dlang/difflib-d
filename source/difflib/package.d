/* SPDX-License-Identifier: Zlib */

/**
 * difflib
 *
 * A reimplementation of Python's difflib in native D.
 *
 * Parts of the Python difflib package have been reimplemented
 * to provide its capabilities and algorithms natively for
 * consumption from D code.
 *
 * Authors: Copyright © 2022 Serpent OS Developers
 * License: Zlib
 */

module difflib;

public import difflib.types;
public import difflib.sequence;
