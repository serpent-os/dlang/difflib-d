/* SPDX-License-Identifier: Zlib */

/**
 * difflib.sequence
 *
 * Sequence Matcher APIs.
 *
 * Authors: Copyright © 2022 Serpent OS Developers
 * License: Zlib
 */

module difflib.sequence;

import std.range : isInputRange, ElementType, enumerate, take, drop;
import std.array : byPair;
import std.functional : unaryFun;
import std.container.rbtree : RedBlackTree;
import std.algorithm : filter, each, multiSort, fold;
import std.exception : assumeWontThrow;

public import difflib.types;

/**
 * A SequenceMatcher can be used to perform matches against
 * two input ranges
 *
 * Notable difference from python: "A" is our primary, *not* "B".
 * Yes, I really am that awkward.
 *
 * Params:
 *      junkFunc    = Unary function returning true if element is junk
 *      T           = First sequence type to match against
 *      autoJunk    = Apply junk heuristics
 */
public class SequenceMatcher(T, alias junkFunc = "false", bool autoJunk = true)
        if (isInputRange!T)
{

    @disable this();

    /**
     * Construct a SequenceMatcher from the given sequences
     *
     * Params:
     *      primary    = Primary (baked) range
     *      secondary     = Secondary (check) range
     */
    this(in SequenceType primary, in SequenceType secondary) @safe
    {
        junkElements = new RedBlackTree!TypeElement;
        this._primary = cast(SequenceType) primary;
        this._secondary = cast(SequenceType) secondary;
        computeChain();
    }

    /* Python API expectations */
    alias a = primary;
    alias b = secondary;

    /**
     * Primary sequence
     *
     * Returns: Primary sequence
     */
    pure @property SequenceType primary() @safe @nogc nothrow
    {
        return _primary;
    }

    /**
     * Secondary sequence
     *
     * Returns: Secondary sequence
     */
    pure @property SequenceType secondary() @safe @nogc nothrow
    {
        return _secondary;
    }

    /**
     * Update the secondary sequence
     *
     * Params:
     *      b = Secondary sequence
     */
    pure @property void secondary(in SequenceType b) @trusted @nogc nothrow
    {
        _secondary = cast(SequenceType) b;
    }

    /**
     * Try to find the longest match
     *
     * If no match can be found, the return will be isNull()
     *
     * Params:
     *      offsets = Optional offsets to perform our match
     * Returns: A Nullable!Match
     */
    pure OptionalMatch longestMatch(MatchOffsets offsets = MatchOffsets.init) @safe
    {
        const auto ahi = offsets.a.hi == 0 ? a.length : offsets.a.hi;
        const auto bhi = offsets.b.hi == 0 ? b.length : offsets.b.hi;

        ulong bestI = offsets.a.lo;
        ulong bestJ = offsets.b.lo;
        ulong bestSize;

        ulong[ulong] junkLengths;

        foreach (i, element; a.enumerate.take(ahi).drop(offsets.a.lo))
        {
            foreach (j; assumeWontThrow(junkMap.get(element, null)))
            {
                ulong[ulong] newJunkLengths;

                if (j < offsets.b.lo)
                {
                    continue;
                }
                else if (j >= bhi)
                {
                    break;
                }
                immutable k = newJunkLengths[j] = assumeWontThrow(junkLengths.get(j - 1, 0)) + 1;
                if (k > bestSize)
                {
                    bestI = i + 1 - k;
                    bestJ = j + 1 - k;
                    bestSize = k;
                }
                junkLengths = newJunkLengths;

            }
        }

        while (bestI > offsets.a.lo && bestJ > offsets.b.lo
                && !hasJunk(b[bestJ - 1]) && a[bestI - 1] == b[bestJ - 1])
        {
            bestI--;
            bestJ--;
            bestSize++;
        }

        while (bestI + bestSize < ahi && bestJ + bestSize < bhi
                && !hasJunk(b[bestJ + bestSize]) && a[bestI + bestSize] == b[bestJ + bestSize])
        {
            bestSize++;
        }

        while (bestI > offsets.a.lo && bestJ > offsets.b.lo
                && hasJunk(b[bestJ - 1]) && a[bestI - 1] == b[bestJ - 1])
        {
            bestI--;
            bestJ++;
            bestSize++;
        }

        while (bestI + bestSize < ahi && bestJ + bestSize < bhi
                && hasJunk(b[bestJ + bestSize]) && a[bestI + bestSize] == b[bestJ + bestSize])
        {
            bestSize++;
        }

        return OptionalMatch(Match(bestI, bestJ, bestSize));
    }

    /**
     * Return a range of all matching blocks between primary and secondary
     *
     * Returns: Array of Match() for both sequences
     */
    auto matchingBlocks() @safe
    {
        const auto lenA = a.length;
        const auto lenB = b.length;
        import std.range : popFront, empty, front;

        MatchOffsets[] queue = [
            MatchOffsets(Offset(0, lenA), Offset(0, lenB),)
        ];
        Match[] blocks;

        while (!queue.empty)
        {
            MatchOffsets item = queue.front;
            queue.popFront;

            immutable auto matchable = longestMatch(item);
            /* No match, check */
            if (matchable.isNull)
            {
                continue;
            }
            Match match = matchable.get;

            if (item.a.start < match.a && item.b.start < match.b)
            {
                queue ~= MatchOffsets(Offset(item.a.start, match.a),
                        Offset(item.b.start, match.b),);
            }

            if (match.a + match.size < item.a.start && match.b + match.size < item.b.end)
            {
                queue ~= MatchOffsets(Offset(match.a + match.size,
                        item.a.start), Offset(match.b + match.size, item.b.end),);
            }

            blocks ~= match;
        }

        /* Get er sorted */
        blocks.multiSort!("a.a < b.a", "b.a < b.b");

        /* Deal with adjacents */
        Match adjCol;
        Match[] nonAdj;
        foreach (m; blocks)
        {
            if (adjCol.a + adjCol.size == m.a && adjCol.b + adjCol.size == m.b)
            {
                adjCol.size += m.size;
                continue;
            }
            if (adjCol.size != 0)
            {
                nonAdj ~= adjCol;
            }
            adjCol = m;
        }

        /* Catch strays */
        if (adjCol.size != 0)
        {
            nonAdj ~= adjCol;
        }

        nonAdj ~= Match(a.length, b.length, 0);
        return nonAdj;
    }

    /**
     * Return the ratio of similarity between A and B
     *
     * Params:
     *      blocks = Matches from matchingBlocks()
     *
     * Returns: float clamped between 0.0 and 1.0
     */
    pure float ratio(in Match[] blocks) @safe @nogc nothrow
    {
        assert(blocks.length >= 1);

        /* Fold all sizes as a float */
        static immutable float seed = 0.0;
        immutable auto summed = blocks[0 .. $ - 1].fold!((a, b) => a + b.size)(seed);

        immutable double length = a.length + b.length;
        return length > 0 ? 2.0f * (summed / length) : 1.0f;
    }

private:

    /**
     * Return true if the junk element is known
     *
     * Params:
     *      e   = Element to test presence
     * Returns: True if we found the junk element
     */
    pragma(inline, true) pure bool hasJunk(in TypeElement e) @safe nothrow @nogc
    {
        return false;
    }

    /**
     * Compute the chain for B
     */
    void computeChain() @safe
    {
        b.enumerate.each!((i, element) {
            junkMap.update(element, () => [i], (ref ulong[] old) {
                old ~= i;
                return old;
            });
        });

        /* Dejunk everything */
        junkMap.keys
            .filter!((e) => isElementJunk(e))
            .each!((e) => junkElements.insert(e));
        junkElements[].each!((e) => junkMap.remove(e));

        /* handle autojunk policy */
        static if (autoJunk)
        {
            auto popularElements = new RedBlackTree!TypeElement;
            immutable auto len = b.length;

            /* Not interested */
            if (len < 200)
            {
                return;
            }

            /* number to test */
            immutable auto ntest = len / 100.0 + 1;

            /*
             * For all elements with indices > ntest, insert element into
             * the popularElements set
             */
            junkMap.byPair
                .filter!((pair) => pair.value.length > ntest)
                .each!((pair) => popularElements.insert(pair.key));

            /* Remove all popular elements from the junkMap */
            popularElements.each!((e) => junkMap.remove(e));
        }
    }

    alias SequenceType = T;
    alias TypeElement = ElementType!SequenceType;

    SequenceType _primary;
    SequenceType _secondary;

    /* Map to indices of junk elements */
    ulong[][TypeElement] junkMap;

    /* Discovered junk elements */
    RedBlackTree!TypeElement junkElements;

    /* Private alias to filter junk */
    alias isElementJunk = unaryFun!(junkFunc, "a");
}

/**
 * Convenience function to return SequenceMatcher for the ranges
 *
 * Params:
 *      primary       = Primary range to compare
 *      secondary     = Range to be tested
 *      isElementJunk = Unary function to filter out junk
 *      autoJunk      = apply automatic junk heuristics
 *
 * Returns: A SequenceMatcher for the given ranges
 */
public auto sequenceMatcher(alias isElementJunk = "false", T, bool autoJunk = true)(
        T primary, T secondary)
{
    return new SequenceMatcher!(T, isElementJunk, autoJunk)(primary, secondary);
}

/**
 * As yet nothing done with this.
 */
@("Placeholder") @safe unittest
{
    import std.uni : isAlphaNum;

    /**
     * Round inputs to two decimal places
     */
    static auto roundf(in float val)
    {
        import std.math : round, pow;
        static immutable auto power = pow(10.0, 2);
        return round(val * power) / power;
    }

    auto matcher = sequenceMatcher!((e) => !e.isAlphaNum)("text goes here doesn't it",
            "text is here doesn't it just");
    const auto match = matcher.longestMatch();
    assert(match == Match(8, 6, 17));
    const auto blocks = matcher.matchingBlocks();
    assert(blocks == [Match(0, 0, 5), Match(8, 6, 17), Match(25, 28, 0)]);

    immutable auto ratio = roundf(matcher.ratio(blocks));
    assert(ratio == 0.83);
}
