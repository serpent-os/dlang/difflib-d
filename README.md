# difflib-d

A partial port of the Python [difflib](https://docs.python.org/3/library/difflib.html) package to D.

### License

Copyright &copy; 2022 Serpent OS Developers

Available under the terms of the [Zlib](https://spdx.org/licenses/Zlib.html) license.
This is a reimplementation with modification to match the Pythonic API expectations
but isn't internally identical. All credit for the algorithm etc goes to the difflib
developers!
