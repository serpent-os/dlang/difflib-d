import difflib

one = difflib.SequenceMatcher(None, "text goes here doesn't it", "text is here doesn't it just")
mb = one.get_matching_blocks()
mbf = one.find_longest_match()
print(mb)
print(mbf)
print(one.ratio())

two = difflib.SequenceMatcher(None, [1, 2, 3], [0, 1, 2, 4])
mb2 = two.get_matching_blocks()
mbf2 = two.find_longest_match()
print(mb2)
print(mbf2)
